from django.contrib import admin
from .models import Userprofile, Movie

admin.site.register(Userprofile)
admin.site.register(Movie)
