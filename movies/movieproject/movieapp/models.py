from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class CreateModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


class Userprofile(CreateModel):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    role = models.CharField(max_length=20,blank=True)
    mobileNumber = models.BigIntegerField(blank=True)
    avatar = models.CharField(max_length=500, default='/assets/img/avatar/avatars-03.svg')

    def __str__(self):
        return self.role


class Movie(CreateModel):
    profile =models.ForeignKey(Userprofile,blank=True,null=True, on_delete=models.CASCADE)
    type = models.CharField(max_length=500,blank=True)
    name = models.CharField(max_length=50,blank=True, null=True)
    movieUrl = models.CharField(max_length=500,blank=True)
    about = models.TextField(blank=True)
    postDate = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.type
