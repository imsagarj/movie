from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib import messages,auth
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from .models import Movie,Userprofile   
from movieproject import settings
import os
# Create your views here.
def home(request):
    allMovie = Movie.objects.all().order_by('id')

    return render(request,'index.html',{"headMovies":allMovie[0:3],"sigleMovie":allMovie[3:4],"subMovies":allMovie[4:12],"allMovie":allMovie})

@login_required
def landing(request):
    userprofile = request.user.userprofile
    allMovie = Movie.objects.filter()
    print("User is here-->>>",allMovie)
    return render(request,'landing.html',{"allMovie":allMovie})

def signup(request):
    if request.method == "GET":
        return render(request,'signup.html')
    else:
        username = request.POST['username'].split()
        fName = username[0]
        lName = username[1]

        useremail = request.POST['useremail']
        userMobileNumber = request.POST['userMobileNumber']
        userRole = request.POST['userRole']
        userPassword = request.POST['userPassword']

        userObj = User()
        userObj.first_name = fName
        userObj.last_name = lName
        userObj.email = useremail
        userObj.username = useremail
        userObj.set_password(userPassword)
        userObj.is_active = True
        userObj.save()

        user = User.objects.get(username=useremail)
        print("USER VALUE IS HERE-->>",user.id)
        up = Userprofile()
        up.user_id = user.id
        up.mobileNumber = userMobileNumber
        up.avatar = "/assets/img/avatar/avatars-03.svg"
        up.role = userRole
        up.save()
        
        user.backend = settings.AUTHENTICATION_BACKENDS[0]
        auth.logout(request)
        auth.login(request,user)

        return HttpResponseRedirect(reverse('landing'))

def login(request):
    if request.method == "GET":
        return render(request,'login.html')
    else:
        print(request.POST['useremail'],request.POST['userpassword'])
        username = request.POST['useremail']
        password = request.POST['userpassword']

        user = auth.authenticate(username=username,password=password)
        if user:
            auth.login(request,user)
            return HttpResponseRedirect(reverse('landing'))
        else:
            return render(request,'login.html')


def contact(request):
    if request.method == "GET":
        return render(request,'contact.html',{})
    elif request.method == "POST":
        send_mail(request.POST["subject"],request.POST["message"] ,request.POST["email"],os.environ.get("ADMIN_MAIL"))
        messages.success(request,"Mail Send Successfully.")
        return HttpResponseRedirect(reverse('contact'))
    else:
        messages.error(request,"Somethig Went Wrong Please Try Again.")
        return render(request,'contact.html',{})

        
@login_required
def profile(request):
    if request.method == "GET":
        return render(request,'profile.html',{})
    elif request.method == "POST":
        pass
    else:
        return render(request,'profile.html',{})
        
@login_required
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('home'))


@login_required
def movie_add(request):
    return render(request,'movieadd.html',{})